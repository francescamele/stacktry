/**
 * II05'vid
 * 
 * Di norma abbiamo UN'UNICA SORGENTE e tanti ascoltatori
 * 
 * Contesti: eg. chats
 * 
 * Non mando tutto a tutti: abbiamo canali privati. Eg, se ricevo 
 * una serie di ordini, nella mia pagina privata avrò un contatore 
 * che mostra gli ordini che IO ricevo, aggiornati in tempo reale
 * 
 * Come capire quando è effettivament un socket e quando invece 
 * sono io a fare richieste?
 * Look at the socket.js's console:
 * Su instagram, se voglio vedere cose del passato, scrollo e l'app 
 * mi mostra qualcos'altro; ma invece usa i socket quando ti appare 
 * l'icona per i new posts: l'app aspetta che il socket le invii dei 
 * dati, e quando il socket le dice che ci sono, c'è un segnale, cioè 
 * l'immagine di "new posts"; o magari esso stesso ha i file al suo
 * interno, dipende dall'implementazione.
 * Di solito quando la roba si aggiorna da sola, sono socket.
 * Qualsiasi app di messaggistica è fatta coi socket
 * 
 * Idealmente, i socket potrebbero darci info strutturate in modo
 * particolare
 */

//Potrei ricevere:
{
    "chatId": 123,
    "from": 3338691125,
    "message": 'messaggio'
}

//https://www.piesocket.com/websocket-tester