/**
 * 02'
 * Struttura l'arr vuoto per y
 * Poi 
 * 
 * In asse x, metti il Date.now(): timestamp del momento
 * in cui avviene l'evento. Non c'è niente da metterci 
 * di meglio.
 */
import Chart from "chart.js/auto";

const ctx = document.getElementById("beatChart").getContext("2d");

//const timestamps = [];
//const beats = [];

function initChart() {
    return new Chart(ctx, {
        type: 'line',
        data: {
            labels,
            datasets: [{
                label: "My first beat chart",
                data,
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.1
            }]
        },
        options: {
            animation: false,
            scales: {
                x: {
                    beginAtZero: true,
                    max: 140
                },
                y: {
                    beginAtZero: true,
                    max: 100
                }
            }
        }
    });
}
 

//let beat = 1960;
//let timestamp = 10;
let beatChart = initChart();
let data = [];
let i = 1;
let labels = [];

setInterval(() => {
//10' vedi video
    //La prima cosa è contare/stabilire il numero di
    //punti. Lo usiamo per contare, e noi non partiremo 
    //da 0
    //Ciclo che serve a disegnare dei punti sul grafico:
    //Per mettere il valore 50 nell'arr data qui, cosa 
    //devo controllare? -> Devi mettere il valore 50 
    //nell'arr. se stiamo nella prima o nell'ultima 
    //parte del grafico
    //Parte iniziale e finale del grafico
            // for (let i = 1; i <= 140; i++) {
    //^32' devono essere 140 perché... e 35'
    //44' abbiamo scelto il for, che fa tutte le cose insieme ma 
    //a intervalli di un sec: ogni secondo eseguo un for. No good.
    //La gestione dell'indice la dobbiamo fare noi, non il for:
    //farlo da fuori
        if (i <= 20 || i >= 120) {
            data.push(50);
        } else if (i > 45 && i <= 95) {
            //1h41vid per i > 45, NON i >= 45
            //console.log(data[data.length - 1]);
            
            //else if: discesa
            //Prendere l'ultimo valore dell'arr: 
            //1. array.length - 1
            //2. Hai l'indice, quindi puoi usare l'index
            //   Non posso scrivere data[1] per prendere l'ultimo
            //   elem. 29'
            //Il metodo pop ce lo leva, quindi se lo usi poi lo
            //devi rimettere, e poi rimetterlo un'altra volta
            //incrementato di 1.
            //data.length mi dà la lunghezza dell'arr; devo fare
            //-1 perché l'arr parte da 0; ma i parte da 1: quella
            //i io la devo trattare esattamente come data.length,
            //quindi devo scrivere: data[i - 1] preleva l'ultimo el
            //nell'arr
            //Se i conta i punti... (es.)
            //      - Sono al 45esimo punto
            //      - Nell'arr avrò posizioni da 0 a 44 (45 elementi)
            //      - Prelevo la posiz 44 che contiene l'ultimo num:
            //        (i = 45; i - 1 => 44)
            //const last = data[i - 1];
            //^v.35'vid
            //data.push(last - 1);
            data.push(data[data.length - 1] - 1);
        } else {
            //console.log(data.length, i - 1);
            //Faccio la stessa cosa di prima, ma incrementando invece
            //di decrementare
            data.push(data[data.length - 1] + 1)
        }

        //Ogni volta che disegno un punto devo fare due cose:
        //1. 41'vid
        labels.push(i);
        //2. far aggiornare il grafico
        beatChart.update();

        //Togliamo l'indice da qui dentro e ci mettiamo invece un modo
        //per pulire il grafico e resettare l'indice
        if (i === 140) {
            //47'
            //1h09vid
            console.log(data);
            //Senza return:
            i = 0;
            data = [];
            labels = [];
            beatChart.destroy();
            beatChart = initChart();
        }
        i++;
//ultima cosa: a che intervallo di tempo disegnare l'el successivo? Magari
//in un sec si disegnano tutti i 140 pti: facciamo 1000/140 = 7 -> ogni 7''
//si aggiunge un punto (o esageriamo e mettiamo 10)
}, 100);

//Continua a non funzionare: 50' ...se l'arr dei dati continua a contenere
//gli stessi dati, poi continua a buttarci dentro roba: devi rendere data e 
//labels degli arr vuoti



    //data.push(+(Math.random() * 100).toFixed());
    //labels.push(++lastYear);

//     if (timestamp === 20) {
//         timestamp = 10;
//         beat = 1960;
//     } else {
//         data.push(++beat);
//         beats.push(++timestamp);
//     }
//     beatChart.update();
// }, 1000)