//17.12 30'

//Creo una classe di errore personalizzata che estende quella
//base di errore
//Non è un model perché è un errore
export class IndexRelatedError extends Error {
    //^Ho creato un errore (classe) relativo all'indice
    constructor(message) {
        super(message);
    }
}