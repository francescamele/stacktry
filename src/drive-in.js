const menuMobile = document.querySelector('.menu-mobile');
//1h06
document.querySelector(".hamburger-menu").addEventListener("click", e => {
    //Dobbiamo aggiungere una classe, agendo su classList:
    // add, remove, toggle (si prende il nome di una classe: se c'è,
    // va bene, ... 1h10)
    // Toggle: aggiunge una classe se questa NON è presente sull'el 
    // Viceversa, se è già presente la rimuove
    // ...check his notes on Johanna's
    menuMobile.classList.toggle('show');
})