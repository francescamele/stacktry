//13.12
import { Comment } from "../models/comment";

/**
 * Fetch the comment for the given id that needs to be modified
 * 
 * @param {number} id 
 * @returns {Promise<Comment>}
 */
export const updateComment = async id => {
    //Definisco una funzione per prelevare un commento: uno solo, 
    //quindi nome al singolare (updateComment) e mi prendo l'id
    const response = await fetch(
        `http://jsonplaceholder.typicode.com/comments/${id}`,
    );
    const data = await response.json();

    // return new Comment(data.name, data.email, data.body);
    return response.status === 200
        ? new Comment(data.id, data.email, data.name, data.body, data.postId)
        : null;
        //postId te lo manda la risposta ogni volta che prendi il commento: 
        //non ci serve ma visto che le risposte devono essere omogenee e 
        //nella risposta c'è il postId, ce lo lascio
}

/**
 * Create a comment accordignly to the given params
 * 
 * @param {string} email User's email
 * @param {string} name User's name
 * @param {string} body Content of the comment
 * @param {string} postId Id of the post
 * @returns {Promise<Comment | null>} //vedi 3h06'?
 */
export const addComment = async (email, name, body, postId) => {
    //Devo AGGIUNGERE: al fetch dirò che deve lavorare col metodo 
    //POST e che il body di questa richiesta avrà: body, email, name, postId.
    //Usiamo POST (creare) e non GET (leggere) perché dobbiamo 
    //creare qualcosa, non prendere
    //"body" perché lo chiama così il backend: se lo voglio chiamare 
    //"content", devo scrivere: "body: content"
    const response = await fetch(
        `http://jsonplaceholder.typicode.com/posts/${postId}/comments`, 
        {
            body: JSON.stringify({
                //Va aggiunto il JSON.stringify perché sennò non può leggere i dati, 
                //o trovare gli elementi. Dovrebbe esserci errore 404, infatti
                body,
                email,
                name,
                postId
            }),
            headers: {
                //Gli headers lo fanno funzionare: gli serviva il charset per 
                //capire con che codifica gli sto dando la risposta
                //Il content-type dettaglia il contenuto della tua richiesta 
                //espressa/definita nel tuo body
                'Content-Type': 'application/json; charset=utf-8',
            },
            method: "POST",
        }
    );

    //Dobbiamo leggere la risposta: i dati li troviamo all'interno del json
    const data = await response.json();
    //1h58'

    //Vogliamo restituire il commento che ci ha dato il backend, cioè un 
    //commento con gli elementi che gli abbiamo passato.
    return response.status === 201
        ? new Comment(data.id, data.email, data.name, data.body, data.postId)
        : null;
    //data.id perché avrò l'id che ci verrà dato dal backend.
    //L'ordine lo vedo da quello che mi suggerisce VSC
};

//METODO DELETE:
/**
 * Deletes a comment for the given id
 * 
 * @param {number} id The id of the comment to delete
 * @returns {Promise<Comment | null>} Deleted comment
 */
export const deleteComment = async id => {
    //Questa funzione si prenderà un solo parametro, perché è l'id della risorsa
    //che vogliamo eliminare. Infatti questo è riferito al tipo di charset e 
    //content type che hai già nel tuo body: non c'è quindi bisogno di definirli qui
    const response = await fetch(
        `http://jsonplaceholder.typicode.com/comments/${id}`, 
        {
            method: 'DELETE'
        }
    );

    //Mi vado a prelevare il contenuto al solito modo:
    const data = response.json();
    //Se passo id1, tolgo il commento con id 1

    //Di norma, dovresti restituire tutto il commento eliminato:
    return response.status === 200 
    ? new Comment(data.id, data.email, data.name, data.body, data.postId)
    : null;
    //Per response.status: vedi post.js (src)

    //Se voglio eliminare dall'interfaccia un commento, devo aggiungere un bottone 
    //lì vicino con scritto "delete"
    //Lo restituisce perché magari vuoi farci delle cose, con il commento che hai 
    //eliminato, magari mostrarlo all'utente una volta eliminato.
    //E in generale il backend dev'essere CONSISTENTE NELLE RISPOSTE. Quando
    //interroghiamo un endpoint come Comments, ci deve restituire o un oggetto Comment 
    //o un array di oggetti Comment (a meno che non siamo noi a chiedere altre info).
    //Ogni volta che recupero dei commenti, devo fare qualcos'altro: v. post.js (src)
};

/**
 * Fetch the comments for the given post id
 * 
 * @param {number} postId The post Id
 * @returns {Promise<Comment[]>} (This will be an array of comments: [])
 */
export const getComments = async postId => {
    const response = await fetch(
        `http://jsonplaceholder.typicode.com/posts/${postId}/comments`
    );
    const data = await response.json();

    //Se voglio trasformare tanti ogg, di model, non ogg generici:
    //sugli arr, abbiamo il metodo map per applicare una funzione a ogni elemento:
    return data.map(c => new Comment(c.id, c.email, c.name, c.body, c.postId));
    //^c sarà il singolo commento su cui iteriamo. In risposta, vogliamo aggiungere al 
    //nuovo array che stiamo creando un commento
    //Tra l'altro, qui quando scrivi l'arrow function e poi stai per scrivere "Comment",
    //se tieni premuto CTRL quando fai spazio dopo 'new' dovrebbe suggerirti lui il percorso 
    //di Comment
    //Chiaramente, metti il return perché vuoi che la funzione ti restituisca questo 
    //nuovo arrai dove hai applicato la funz

    //Adesso voglio leggere i commenti, quindi devo chiamare getComments in post.js
}

//Il PUT vuole TUTTA L'ENTITA': se voglio modificare il commento, 
//glielo devo dare TUTTO. Se non passi interamente il commento ti
//dà errore classe 400: risposta non strutturata correttamente.
//PUT: solo per visualizzare i dati.
//POST: creare i dati.
//PATCH: non vuole che trasferisci l'intera entità, quando fai la 
//      modifica. 
//      EG: hai tabella con ordini di un e-commerce, con mille
//      dati, tra cui stato dell'ordine: quando vuoi passare quest
//      info, gli passi solo questa info, mentre invece con il PUT
//      gli avresti passato tutto
/**
 * HEAD: serve a prelevare gli header di una richiesta, equivalente 
 * in GET. S
 */
/**
 * 
 * @param {*} id 
 * @param {*} email 
 * @param {*} name 
 * @param {*} body 
 * @param {*} postID 
 * @returns 
 */
export const putComment = async (id, email, name, body, postID) => {
    //I parametri sono le info che ci servono: l'id del commento che
    //vogliamo modificare ecc. Sono le proprietà che di solito vengono
    //inserite nel commento nel model, e sono tutte le proprietà 
    //dell'oggetto comment

    //Dobbiamo far partire la chiamata:
    const response = await fetch(
        //Indirizzo: stesso di quello usato per interrogare il commento
        `http://jsonplaceholder.typicode.com/comments/${id}`, {
            //Dobbiamo descrivere l'oggetto: metodo PUT, che come POST
            //lavora inserendo le info nel body, & needs headers.
            body: JSON.stringify({
                body,
                email,
                id,
                name,
                postID
            }),
            headers: {
                //Il Content-Type serve a spiegare al browser 1h04 
                //Se non gli invii questo, non puoi sapere a prescindere 
                //come si comporta il BE: di norma dovrebbe 
                //lavorare con application/json e utf-8, ma non sempre 
                //(tipo questo no), e quindi quanto lavori con 
                //post, put e fetch USALI SEMPRE: vai a specificare cosa 
                //gli stai passando con questa richiesta.
                //'application/json' = stai lavorando con una stringa
                //charset=utf-8 = questa è la codifica con cui ho scritto
                //    questa stringa
                "Content-Type": "application/json; charset=utf-8",
            },
            method: 'PUT'
        }
    );

    //Vediamo cosa ci restituisce la risposta:
    const data = await response.json();

    //Eventualmente, qui altre operazioni, come la trasformazione della 
    //risposta in un model.
    //Per capire che mi risponde, vedo documentazione di placeholder e 
    //vedo "Modifica": restituisce commento modificato
    return new Comment(data.id, data.email, data.name, data.body, data.postId);
    //La mia chiamata putComment mi restituirà anche il model dell'elem che ho 
    //modificato, così come l'ho modificato ed è stato percepito dal BE
    //Faccio partire chiamata nel submit: vedi comment.js
}