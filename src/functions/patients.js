// import { async } from "regenerator-runtime";

// export async function getPatient() {
//     return fetch('http://localhost:3000/people')
//         .then(response => response.json())
//         .then(patients => patients.map(a => new ))
// }


async function call(e) {
    let response = await fetch(e);

    if (response.ok) {
        let risposta = await response.json();
        console.log(risposta);
    } else {
        alert("HTTP-Error: " + response.status);
    }
}

call('http://localhost:3000/drugs');
call('http://localhost:3000/people');