// const commentUpdating = document.querySelector('form[name="update-comment"]');

import { loadConfig } from "@babel/core/lib/config/files";
import { putComment, updateComment } from "./functions/comments";
import { Post } from "./models/post";

// const updatedComment = async c => {
//     const content = document.querySelector('input[name="content"]');
//     const email = document.querySelector('input[name="email"]');
//     const name = document.querySelector('input[name="name"]');

//     content.innerHTML = c.body;
//     email.innerHTML = c.email;
//     name.innerHTML = c.name;

//     const commentToUpdate = await updateComment(1);
//     //Prendo il commento che mi interessa con await updateComment, 
       //ma non so come separare le sue parti 
//     //nei tre querySelector e quindi nei tre innerHTML
//     //Poi non so come collegare commentUpdating con questa funzione
//     //Poi per richiamarla, mi basta scrivere "updatedComment();" più 
//       in basso???
// }

//FIRST:
//individio i singoli campi in input all'interno del form:

//Form
const form = document.querySelector('form[name="edit-comment"]');

//Form's input fields
const content = document.querySelector('input[name="content"]');
const email = document.querySelector('input[name="email"]');
const id = document.querySelector('input[name="id"]');
const name = document.querySelector('input[name="name"]');
const postId = document.querySelector('input[name="postId"]');


//Una volta individuati i campi in input nel form, possiamo accedervi 
//per individuarne i valori

//La prima cosa che facciamo è far partire la chiamata: così 
//preleviamo il commento e andiamo a 
//mettere i suoi lavori all'interno dei campi
// updateComment(3);

//Ci vuole la funzione asincrona, altrimenti dovresti lavorare coi then
const load = async () => {
    const comment = await updateComment(3);

    //Imposto i campi in input del form con i valori che ho ricevuto 
    //dalla chiamata
    content.value = comment.body;
    email.value = comment.email;
    id.value = comment.id;
    name.value = comment.name;
    postId.value = comment.postId;
    //A sx c'è l'elemento che deve contenere il valore; a dx c'è il valore
    //di riferimento: il campo in input, alla sua proprietà value, dovrà
    //contenere/essere uguale a comment.x, dove x è collegato al parametro
    //di updateComment e quindi sarà il dato da mostrare nel campo.
    //Dentro value c'è quello che scrive l'utente: se modifico l'attributo
    //value, modifico il content dell'attributo input

    //Potete ascoltare i cambiamenti in un campo input. Eg: ammetto 
    //solo vocali, non consconanti. 
    //Se modifichi il value in tempo reale, l'utente poi vede lettere 
    //che si aggiungono o tolgono in tempo reale: sincronia tra quello
    //fa l'utente e quello che faccio io, che ho l'ultima parola

    //Per verificare che i campi in input nascosti ricevano correttamente
    //l'info: "ispeziona elemento": scorri nel codice e vedi i value nei 
    //campi in input nascosti
}

//Visto che so che devo invocarla, lo faccio subito, prima ancora di 
//scrivere il body della funz
load();

form.addEventListener("submit", async e => {
    //thr param () mi prendo il payload dell'evento
    e.preventDefault();

    //Qui dentro dovremmo idealmente far partire la chiamata al 
    //servizio esterno. In particolare, vai su comments.js (functions)
    const comment = await putComment(id.value, email.value, name.value, content.value, postId.value);
    //Anche questa dev'essere una funz asincrona
    console.log(comment);
    //^Stampo il commento per vedere che mi tira fuori il backend: 
    //capisco che la richiesta è stata trattata 
    //correttamente quando mi dà un oggetto Comment in risposta 
    //(anche se non è stato modificato perché 
    //questo è solo un test)
})