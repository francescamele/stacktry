//Questo file ci permette di gestire le chiamate

import { async } from 'regenerator-runtime';
import { addComment, deleteComment, getComments } from './functions/comments';
import { getPost } from './functions/posts';

//Ricordati: le costanti non sono soggette a hoisting, quindi vano dichiarate qui 
//sopra, prima di essere richiamate. E le scrivi in ordine alfabetico

//1h38'
const addCommentForm = document.querySelector('form[name="add-comment"]');
//Poi dobbiamo aggiungere ascolatatore evento per submit

//Individuo elem html: sezione comments che ho appena creato
const commentsSection = document.querySelector('#comments');
//Poi comments.js

//Per trovare il primo figlio dell'el. di una classe, uso il querySelector e scrivo:
//nome classe > nome tag elemento contenuto nell'el con quella classe
//Quindi: .post > p oppure .post > h1
//Per trovare il contenuto:
const postContent = document.querySelector('.post > p');
//Per trovare l'h1:
const postTitle = document.querySelector('.post > h1');

const appendComment = c => {
    // commentsSection.innerHTML += `
    //     <div class="row">
    //         <div class="col.lg-12">
    //             <h3>${c.name} <span class="delete-comment" data-id="${c.id}">X</span></h3>
    //             <p>${c.body}</p>
    //             <p>${c.email}</p>
    //         </div>
    //     </div>
    // `;
    //^span class="delete comment": per poter eliminare l'elemento
    //^data-id: è un data attribute, vedi slide. 

    //DATA ATTRIBUTE
    //L'id dev'essere univoco, non posso avere due id uguali: data-id crea un attributo 
    //customed di tipo id, ma non è un id di tipo html, bensì un attributo con nomi 
    //arbitrari che non andranno mai in conflitto con le norme html.
    //Quindi quando farò clic sullo span, mi cercherò nei suoi data attribute un id, 
    //lo troverò e dirò al backend di eliminare il commento con quell'id
    
    //Come ascoltare l'evento di eliminazione per questo span qui? Cambio la funzione 
    //appendComment: creo un elemento div che sarà "<div class="row">"
    const row = document.createElement('div');

    row.className = 'row';
    //^questo sarà l'elemento creato

    row.innerHTML = `
        <div class="col.lg-12">
            <h3>${c.name} </h3>
            <p>${c.body}</p>
            <p>Scritto da: ${c.email}</p>
        </div>
    `;
    
    //Mi creo poi la colonna: 
    const span = document.createElement('span');
    span.className = 'delete-comment';
    //Come valore gli diamo l'id del post che stiamo creando:
    span.setAttribute('data-id', c.id);
    span.innerText = '(X)';

    span.addEventListener("click", async e => {
        //Tutti i data attribute vengono agglomerati in una chiave "dataset" presente
        //sull'elemento html
        //Se creiamo un data-attribute
        console.log(e.target.dataset.id);
        //Invochiamo deleteComment, a cui passiamo l'id del commento che vogliamo eliminare
        //Visto che tutte le funzioni sono closure, potrei anche semplicemente passargli c.id
        //deleteComment()

        //Quando devo eliminare un commento con id x, posso ricavarmi x in due modi:
        //1. Sfruttando il data attribute di id: e.target.dataset.id
        //2. Tenendo in consideraz che tutte le funz in JS sono closure: anche nella
        //   funz di qst ascoltatore posso accedere al param 'c' che contiene l'ogg Comment
        //Rendo la funz asincrona, così posso fare:
        const deletedComment = await deleteComment(e.target.dataset.id);

        //Quando è stato rimosso l'el dal backend (e non stiamo facendo controlli, stiamo dando
        //per scontato che l'elemento verrà rimosso), rimuovo la riga corrispondente al commento 
        //rimosso dal backend
        //Per farlo, di nuovo, due modi:
        //1. Individuo la riga del commento cercando lo span con un particolare data-id (che
        //   coincide con l'id del commento da eliminare)
        //2. Sfrutto le closure: se devo rimuovere la riga, invoco il metodo remove() dalla
        //   costante row, che posso leggere perché sta fuori: ho visibilità su row, già ce l'ho
        console.log(deletedComment);

        //Per sapere se l'eliminazione è andata a buon fine o no, devi guardare il codice, 
        //magari con return response.status... (v. comments.js (functions)): se è null, lo 
        //leggerai lì
        //Se non ottengo un oggetto (codice diverso da 200), la risorsa non è stata rimossa dal 
        //backend, quindi non la rimuovo dall'interfaccia
        if (!deletedComment)  {
            return;
        }

        row.remove();
        //E così ho fatto eliminaz sia lato backend che frontend
        //Per capire
    });

    //A partire da questa riga, mi prendo l'el h3: prima prendo la riga, poi l'h3:
    row.firstElementChild.firstElementChild.append(span);

    commentsSection.append(row);
}

//Come posso leggere una risposta, se questo restituisce una promise?
//const post = getPost(1)
//Uso il then:
// getPost(1).then(...)

//Devo leggere i commenti: devo chiamare getComments, che essendo asincrona mi fa
//usare async e await: vedi sopra
const readComments = async () => {
    //Così mi creo una sezione dove prendere i commenti, passandole l'id del post:
    const comments = await getComments(1);
    //E aggiungo getComments agli import su^

    //Per ogni commento devo prendere la sezione dei commenti, 
    //e per ogni commento aggiungere ro ba:
    comments.forEach(c => 
        //{
        // commentsSection.innerHTML += `
        // <div class="row">
        //     <div class="col.lg-12">
        //         <h3>${c.name}</h3>
        //         <p>${c.body}</p>
        //         <p>${c.email}</p>
        //     </div>
        // </div>`
        //}
        appendComment(c));
};

//Voglio usare async e await, perché una funz async avvolge il suo return
//in una Promise.
//Questo readPost mi restituirà il post
const readPost = async () => {
    const post = await getPost(1);

    postContent.innerHTML = post.body;
    postTitle.innerHTML = post.title;
    //^body e title sono proprietà della classe Post (insieme a id e userId)
}

readPost();
readComments();
updatedComment();


addCommentForm.addEventListener("submit", async e => {
    //Blocco il comportamento del browser:
    e.preventDefault();

    //Raccolgo i dati da inviare al backend, le info che ha digitato l'utente:
    const content = document.querySelector('input[name="content"]').value;
    const email = document.querySelector('input[name="email"]').value;
    const name = document.querySelector('input[name="name"]').value;
    //Scrivo .value perché non voglio il campo ma il valore digitato dall'utente
    //A questo punto verifico quello che c'è in content, email e name, col console.log
    //Solo se mi legge i dati che inserisco nel form vado avanti

    //Dopo aver scritto la funzione addComment in comments.js ('/functions/'), torno qui
    //Aggiungi il commento e preleva la risposta dal backend
    const c = await addComment(email, name, content, 1);
    // commentsSection.innerHTML += `
    //     <div class="row">
    //         <div class="col.lg-12">
    //             <h3>${c.name}</h3>
    //             <p>${c.body}</p>
    //             <p>${c.email}</p>
    //         </div>
    //     </div>
    //     `;
    // })
    appendComment(c);
});