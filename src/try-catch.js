//17.12 vid rec

import { Collapse } from "bootstrap";
import { Collection } from "./models/collection";

let attempts = 1;
let socket;

const connect = url => {
    let hasError = false;

    try {
    // Tenta di fare la chiamata
    //blocco di codice che, potenzialmente, potrebbe generare un errore
    // Gli errori vengono anche chiamati eccezioni
        socket = new WebSocket(url);
    //^indirizzo inserito dall'utente
    } catch (error) {
    //^In 'error'^ ci sarà un ogg con tutte le info sull'errore,
    //automaticamente, ma non lo usiamo per lo sviluppo del try
    //and catch. (Puoi chiamarlo come vuoi)

    // Blocco di code da eseguire se si verifica un errore nelle 
    // istruzioni nel blocco "try"

    // L'err viene intercettato, inserito nella var error, e non
    // risulterà bloccante per l'applicativo

    //Avviso l'utente
        alert('Connessione al socket fallita');
    //Faccio un tentativo in più:
        attempts++;
    //Dico che si è verificato un errore:
        hasError = true;
    } finally {
    // Blocco di code che eseguiamo sia al verificarsi di un errore, sia
    // se il codice in "try" non generi eccezioni

    //Magari intercetto l'err, capisco che è un problema di disponibilità
    // del socket, che magari è impegnato, e riporvo
        if (attempts <= 3 && hasError) {
            //Se ho fatto meno di 4 tentativi e si è verificato un 
            //errore, riprovo a connettermi
            connect(url);
            return;
        }
    }
}

//Es. con classe Collection
const c = new Collection();
c.add('a'); //0
c.add('b'); //1
c.add('c'); //2

//c.get(30); // L'indice 30 non esiste
//40'
try {
    c.get(30);
} catch (error) {
    alert("L'indice selezionato non esiste");
}