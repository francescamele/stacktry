export class Post {
    constructor(id, title, body, userID) {
        this.body = body;
        this.id = id;
        this.title = title;
        this.userID = userID;
    }
}
