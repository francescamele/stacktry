//17.12 50' vid

/**
 * ...
 * 
 * getMessages restituisce i messaggi.
 * NON mi dà accesso alla proprietà, che rimane privata, ma crea
 * una copia dell'arr, così l'originale non verrà toccato
 * 
 * sendMessage: ...
 * 
 * Gli ogg allow me to limit the access to API a chi sta fuori
 */

export class Chat {
    #_messages = [];
    #_socket;

    constructor(url, onMessage) {
        this.#_socket = new WebSocket(url);

        this.#_socket.addEventListener("message", ev => {
            this.#_messages.push(ev.data);
            onMessage(ev);
        });
    }

    getMessages() {
        return [...this.#_messages];
    }

    sendMessages(message) {
        
    }
}