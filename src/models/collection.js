//Vid 17.12 22'

import { IndexRelatedError } from "../errors/collection-errors";

//Creo una classe con all'interno un arr e dei metodi
//per eg add els, prelevare els a partire dall'indice

export class Collection {
    #_elements = [];

    add(e) {
        this.#_elements.push
    }

    get(i) {
        //Questo metodo get è fatto per restituirti un 28' ogg
        //nell'arr
        //Questo metodo si prende un indice, che uso per prendermi
        //un el nell'arr 34'
        const e = this.#_elements[i];

        if (e) {
            return e;
        }

        // Gli errori (eccezioni) sono ogg istanza Error: si 
        // sollevano (rise) o lanciano (throw)
        // L'istruzione da usare è di solito throw, e nel gergo
        // informatico si dice "to rise an exception"

        //Error prende un messaggio che dettaglia l'err che sto 
        //lanciando:
        //Classe error di JS:
        //throw new Error('Element not found for the given id');
        
        // Quando si scatena l'err?
        //  - get prende un indice
        //  - l'indice viene usato per prendere un el dall'arr
        //  - se l'el alla posizione i non esiste, lancio un errore

        //vedi collection-error.js in (errors): voglio lanciare
        //un errore specifico degli index
        throw new IndexRelatedError('Element not found for the given index');
    }
}