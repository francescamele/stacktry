export class Wind {
  constructor(speed, degrees, gust) {
    this.degrees = degrees;
    this.gust = gust;
    this.speed = speed;
  }
}
