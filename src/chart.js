//Non possiamo creare noi l'oggetto, bella per te: lo importi da chart.js
import Chart from "chart.js/auto";
//^Ce la siamo tirata dietro facendo npm install chart.js. Adesso infatti
//c'è una cartella "chart.js" in node_modules
//Non serve scrivere il path di chart.js perché, quando non scrivo il 
//percorso assoluto se lo va a cercare automaticamente in node_modules

//Import preso da 
//https://www.chartjs.org/docs/latest/getting-started/integration.html

//Il metodo getContext preleva il contesto all'interno del tag canvas 
//in cui possiamo disegnare (tutto il canvas).
//JS include funzionalità per fare disegni in un canvas: sono funzioni 
//prevalentemente geometriche, con cui possiamo disegnare linee, cerchi

//Prelevo il contesto del canvas nel quale poter disegnare
const ctx = document.getElementById("chart").getContext('2d');
//Perché 2d? Perché potresti anche lavorare con il 3d, quindi devi specificarlo. 
//A questo grafico basta il contesto 2d perché lavoriamo su assi x e y, magari 
//avremo più linee, tendenzialmente sarà un grafico con andamenti

//Copio-incollo dalla doc:
// const myChart = new Chart(ctx, {
//     //Type: tipo di grafico che sto disegnando
//     type: 'bar',
//     data: {
//         //labels: è un arr, lavorerà per ordine
//         labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
//         //dataset anche è un arr: il rosso ha il 12, il blu ha il 19, ecc,
//         //perché lavoro per posizione, posizionalmente
//         datasets: [{
//             //label: etichetta 
//             label: '# of Votes',
//             data: [12, 19, 3, 5, 2, 3],
//             //Anche i colori di sfondo sono un arr: al primo el, il 12, al secondo
//             //ne avrà 19 ecc
//             backgroundColor: [
//                 'rgba(255, 99, 132, 0.2)',
//                 'rgba(54, 162, 235, 0.2)',
//                 'rgba(255, 206, 86, 0.2)',
//                 'rgba(75, 192, 192, 0.2)',
//                 'rgba(153, 102, 255, 0.2)',
//                 'rgba(255, 159, 64, 0.2)'
//             ],
//             //bordo delle barre del grafico
//             borderColor: [
//                 'rgba(255, 99, 132, 1)',
//                 'rgba(54, 162, 235, 1)',
//                 'rgba(255, 206, 86, 1)',
//                 'rgba(75, 192, 192, 1)',
//                 'rgba(153, 102, 255, 1)',
//                 'rgba(255, 159, 64, 1)'
//             ],
//             borderWidth: 1
//         }]
//     },
//     options: {
//         scales: {
//             y: {
//                 beginAtZero: true
//             }
//         }
//     }
// });

//Chart data
const data = [];
const labels = [];

const chart = new Chart(ctx, {
    //Type: tipo di grafico che sto disegnando
    type: 'line',
    data: {
        //labels: è un arr, lavorerà per ordine
        labels,
        //dataset anche è un arr: il rosso ha il 12, il blu ha il 19, ecc,
        //perché lavoro per posizione, posizionalmente
        datasets: [{
            label: 'My First Dataset',
            data,
            fill: false,
            borderColor: 'rgb(75, 192, 192)',
            tension: 0.1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

let lastYear = 1960;

setInterval(() => {
    //Ci metti un numero casuale, per 100 così mi sposta la virgola di due cifre a 
    //destra e ho un numero > 0, toFixed mi toglie poi tutti i numeri dopo la virgola
    data.push(+(Math.random() * 100).toFixed());
    labels.push(++lastYear);
    //Per aggiornare il grafico:
    chart.update();
}, 1000);
