function User(id, name, email) {
    this.email = email;
    this.id = id;
    this.name = name;
}

var users = [
    new User(123, 'Doctor Who', 'bestdoctor@domain.com'),
    new User(234, 'Amy Pond', 'amypond@domain.com'),
    new User(345, 'River Song', 'riversong@domain.com'),
    new User(456, 'Rory Williams', 'rorypond@domain.com'),
    new User(567, 'Rose Tyler', 'rosetyler@domain.com')
];

users.forEach(function(u) {
    delTable.innerHTML += `
    <tr id="${u.id}">
        <td class="tdboh">${u.id}</td>
        <td class="tdWidth">${u.email}</td>
        <td class="tdWidth">${u.name}</td>
        <td class="specialB"><button>Elimina</button></td>
    </tr>`
})

var buttons = document.querySelectorAll('button');
buttons.forEach(function(b) {
    b.addEventListener('click', function(e) {
        /**
         * r: riferimento alla riga sulla quale l'utente ha cliccato il bottone 
         * */
        var r = e.target.parentElement.parentElement;
        var userId = r.id;

        r.remove();

        var pos = users.findIndex(function(u) {
                return u.id == userId;
        });
        users.splice(pos, 1);
        console.log(users);
    })
})

/**
 * invochi arraysplice(a, b);
 * splice: data la posizione, elimini tutto ciò che c'è a destra.
 * a è la posizione, b è il numero di elementi che vuoi eliminare (se non 
 * metti il valore b, ti elimina tutti gli elementi dell'array)
 * a si trova con un altro metodo degli array:
 * array.indexOf:
 * restituisce valori da 0 a n se trova qualcosa, -1 se non trova niente
 * 
 * Però visto che questo è uno scenario diverso, si usa:
 * findIndex(
 *   function (u) {
 *      
 *   }
 * )
 * (u è l'utente su cui sto iterando)
 * 
 */
/**
 * ForEach: metodo contenuto negli array
 * Ci vogliono le parentesi tonde: metodi e funz. vanno invocati
 * Vuole una funzi. da invocare per ogni elemento dell'array
 * Quando il forEach esegue una funz su un el, mi dà quell'elemento:
 * b è l'elemento che mi viene popolato in automatico da for Each:
 * mi itera il secondo? --> mi dà un'istanza del secondo
 * 
 * 
 * ^Prende una stringa e ?
 * selettori CSS:
 * - tag html: p
 * - classe: .classe
 * - id: #id
 * querySelectorAll vuole un selettore CSS: gli dico che voglio i
 * bottoni
 * 
 * document.querySelectorAll('td > button')
 * ^il segno > è tipo una freccia: prendi i pottoni dentro i td
 * 
 * document.querySelectorAll('button.delete')
 * ^con la classe
 */



/*function boh(e) {
    //e.target.delete();
    e.parent.specialTr;
}

specialButt.addEventListener('click', function(e){
    boh(e);
    //e.parent.parent.delete();
});
specialTr.addEventListener('click', function(e){
    boh(e);
    specialTr.remove;
})*/

// function levateDalCa (riga) {
//     var el = riga.parentNode.parentNode.rigaIndex;
//     document.getElementById("delTable").deleteRow(el);
//     document.getElementById("specialButt").deleteRow(el);
// };
