const path = require('path');

module.exports = {
    devtool: 'eval-source-map',
    entry: {
        'try-catch': './src/try-catch.js',
        socket: './src/socket.js',
        beatChart: './src/beatChart.js',
        chart: './src/chart.js',
        comment: './src/comment.js',
        'drive-in': './src/drive-in.js',
        'ham-menu': './src/ham-menu.js',
        main: './src/app.js',
        meteo: './src/meteo.js',
        post: './src/post.js',
        tabellaDelete3: './src/tabellaDelete3',
        users: './src/users.js',
        whatsapp: './src/whatsapp.js'
    },
    mode: 'development',
    module: {
        rules: [{
            exclude: /node_modules/,
            use: [{
                loader: 'babel-loader'
            }],
            test: /\.jsx?$/,
        }]
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].bundle.js'
    },
}