/**
 * 14/12
 * 
 * SOCKET
 * Sono canali di trasmissione che RIMANGONO APERTI, tra FE e BE. 
 * Il BE mi darà delle info ogni tot tempo, e ogni volta che le ricevo 
 * le elaboro in un certo modo. 
 * L'intervallo varia, potrebbe essere ogni cento millisecondi, o ogni 
 * 5 secondi. Non dipende dal FE né possiamo saperlo da qui.
 * Eg: Youtube: ha un socket aperto e si prende ogni tot una quantità
 * di dati, e piano piano mi riempirò la barra.
 * Non è che mi arrivano necessariamente ordinati, i dati: lavoro per 
 * chunk, blocchi di dati, ognuno dei quali ha un indice numerico che 
 * mi dà l'ordine dei dati.
 */
//Il s. lavora sempre ad eventi.
//1. Si collega a una risorsa
// Create WebSocket connection.
const socket = new WebSocket('ws://localhost:8080');

//2. Apro connessione -> posso fare qualcosa, eg. inviare info
//Aggiungo ascoltatori, così quando si aprirà la connessione 
//invierò qualcosa
// Connection opened
socket.addEventListener('open', function (event) {
    socket.send('Hello Server!');
});

//3. Ascolto info -> ho sempre un evento, che si chiama message,
//a cui passo la funzione. Questa ha un param, event (la nostra e),
//e le info che ci invia il BE, cioè il nostro socket, sono 
//contenute in event.data
// Listen for messages
socket.addEventListener('message', function (event) {
    console.log('Message from server ', event.data);
});


//clearRect serve a pulire lo schermo: gli dai i parametri che 
//dicono da dove partire a dove arrivare per pulire


//Nella doc: https://www.chartjs.org/docs/latest/getting-started/integration.html
//prendiamo 
//Se vado su Chart Types - Line chart, vedo che come type c'è line, 
//quindi capisco a cosa corrisponde "type"

//Poi vai su Developers - API e vedi il metodo update

